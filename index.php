<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <style type="text/css">
            ul{
                list-style-type:none;
                border:1px dotted #555;
                padding:10px;
            }
            ul li{
                padding:0;
            }
            #connection-status{
                color:red;
                font-weight:bold;
            }
            #connection-status.connected{
                color: #0d8c00;
            }
        </style>
        <script src="jquery.min.js"></script>
        <script src="socketio.js"></script>
        <script type="text/javascript">
            var socket = null;
            var apiUrl = 'http://locate.msandbox.co.za/api/'
            var socketUrl = 'http://locate.msandbox.co.za';
            var uuid = getUUID();
            $(document).ready(function() {
                socket = io.connect(socketUrl);
                socket.on('connect', function(m) {
                    $('#connection-status').addClass('connected').html('connected');
                    console.log('Connected to server');
                    console.log(socket.id);
                    socket.emit('uuid', {uuid: uuid});
                });
                socket.on('disconnect', function() {
                    $('#connection-status').removeClass('connected').html('not-connected');
                    console.log('Disconnected from server');
                });
                socket.on('cMessageResponse', function(m) {
                    console.log('cMessageResponse' + JSON.stringify(m));
                });
                socket.on('error', function(e) {
                    console.log(e);
                });
                handleCommands();
            });
            function handleCommands() {
                $('#enable-gps').on('click', function(e) {
                    e.preventDefault();
                    if (socket == null || !socket.connected) {
                        alert('Not connected to server, cannot send commands');
                        return;
                    }
                    postForSocket({command: 'enable-gps'});
                });
                $('#get-gps').on('click', function(e) {
                    e.preventDefault();
                    if (socket == null || !socket.connected) {
                        alert('Not connected to server, cannot send commands');
                        return;
                    }
                    postForSocket({command: 'get-gps'});
                });
                $('#disable-gps').on('click', function(e) {
                    e.preventDefault();
                    if (socket == null || !socket.connected) {
                        alert('Not connected to server, cannot send commands');
                        return;
                    }
                    postForSocket({command: 'disable-gps'});
                });
            }
            function postForSocket(data) {
                var url = apiUrl + "send-command";
                console.log('url: ' + url);
                var d = mergeObjects(data, {uuid: uuid});
                console.log('d: ' + JSON.stringify(d));
                $.ajax({
                    type: "POST",
                    url: url,
                    // The key needs to match your method's input parameter (case-sensitive).
                    data: JSON.stringify(d),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function(data) {
                        console.log('success: ' + JSON.stringify(data));
                    },
                    failure: function(errMsg) {
                        console.log('failure: ' + errMsg);
                    }
                });
            }
            function mergeObjects(obj1, obj2) {
                var obj3 = {};
                for (var attrname in obj1) {
                    obj3[attrname] = obj1[attrname];
                }
                for (var attrname in obj2) {
                    obj3[attrname] = obj2[attrname];
                }
                return obj3;
            }

            function guid() {
                function s4() {
                    return Math.floor((1 + Math.random()) * 0x10000)
                            .toString(16)
                            .substring(1);
                }
                return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
                        s4() + '-' + s4() + s4() + s4();
            }

            function getUUID() {
                var uuid = localStorage.getItem('uuid');
                if (uuid === null) {
                    uuid = guid();
                    localStorage.setItem('uuid', uuid);
                }
                return uuid;
            }
        </script>
    </head>
    <body>
        <span id="connection-status">not-connected</span>
        <ul id="commands">
            <li>
                <a id="enable-gps" href="#">Enable GPS</a>
            </li>
            <li>
                <a id="get-gps" href="#">Get GPS</a>
            </li>
            <li>
                <a id="disable-gps" href="#">Disable GPS</a>
            </li>
        </ul>
    </body>
</html>
